####################################################################################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: save the result in the appropriate DB
#
# created by : marc.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# updated on : 2019-03-12
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################

library(data.table)
library(sparklyr)
library(dplyr)
library(sparkLearning)

saveEngagementResult <- function(sc, sparkDBconURL, sparkDBconURL_l, con, con_l, result, BUILD_UID, RUN_UID, configUID, versionUID, isNightly)
{
    library(futile.logger)

    flog.info("Now saveEngagementResult ...")
    
    # cache result
    flog.info("caching result")
    result <- sdf_register(result, "result_all")
    tbl_cache(sc, "result_all")
    result <- tbl(sc, "result_all")

    if (is.null(result) || sdf_dim(result)[1] == 0)
    {
       flog.info("engagement result is empty. Nothing to save!")
       return (0)
    }

    if (isNightly) # nightly run will update DSE table
    {

      flog.info("Rows in engagement result: %s", sdf_nrow(result))

      # remove column from result
      result <- result %>% select(-learningBuildUID)

      # rename columns
      result <- dplyr::rename(result, repId = repUID, accountId = accountUID)

      # convert data types
      result <- result %>% mutate(repId = as.integer(repId), accountId = as.integer(accountId))

      nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
      result <- result %>% mutate(runDate = date_add(today, 1), createdAt = nowTime, updatedAt = nowTime)

      # write back to table (overwrite)
      flog.info("truncate RepAccountEngagement")
      dbGetQuery(con, "TRUNCATE TABLE RepAccountEngagement")
      
      flog.info("save to RepAccountEngagement")
      sparkWriteToDB(sparkDBconURL, result, tableName="RepAccountEngagement", ifExists='append')
      
    }
    else # manual run will update Learning DB table
    {
      repIdMap <- sparkReadFromDB(sc, sparkDBconURL, "SELECT r.repId, r.externalId FROM Rep as r JOIN RepTeamRep using (repId) JOIN RepTeam rtr using (repTeamId) WHERE isActivated=1", name="repIdMap", partitionColumn="repId")

      accountIdMap <- sparkReadFromDB(sc, sparkDBconURL, "SELECT accountId, externalId FROM Account WHERE isDeleted=0", name="accountIdMap", partitionColumn="accountId")

      result <- dplyr::rename(result, accountId = accountUID, repId = repUID)

      # replace accountId with accountUID
      result <- result %>% inner_join(accountIdMap, by="accountId")
      result <- dplyr::rename(result, accountUID = externalId) %>% select(-accountId)

      # replace repId with repUID
      result <- result %>% inner_join(repIdMap, by="repId")
      result <- dplyr::rename(result, repUID = externalId) %>% select(-repId)

      # add updtedAt & createdAt
      nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
      result <- result %>% mutate(createdAt = nowTime, updatedAt = nowTime)
      
      # delete old data first
      flog.info("delete old RepAccountEngagement")
      SQL <- sprintf("DELETE FROM RepAccountEngagement WHERE learningBuildUID='%s'", BUILD_UID)
      dbGetQuery(con_l, SQL)  # remove old build records in Learning DB

      # save data into RepAccountEngagement table in Learning DB.
      flog.info("save to RepAccountEngagement")
      sparkWriteToDB(sparkDBconURL_l, result, tableName="RepAccountEngagement", ifExists='append')
    }
    
    flog.info("Finish saveEngagementResult")

}

from pyspark import SparkConf
from pyspark.context import SparkContext
from awsglue.context import GlueContext
import pandas as pd
#from delta.tables import *
from dataLoading import Data_loading
from awsglue.utils import getResolvedOptions
from utils import Utils
from initRecordClass import InitRecordClass
from dseRecordClassTrans import DseRecordClassTrans
from stRecordClassTransCri import StRecordClassTransCri
from finalDataTrans import FinalDataTrans
from suggestionsProductTrans import SuggestionsProductTrans
import os
from datetime import datetime
import json
import sys

if __name__ == '__main__':
    #  Getting Glue, Spark, Sql context and setting conf for delta
    now = datetime.now()
    date_time = now.strftime("%Y-%m-%d")
    print("The script is running on ", date_time)
    # create a glue Context and connect that with spark
    conf = SparkConf(loadDefaults=True)
    conf.set("spark.delta.logStore.class", "org.apache.spark.sql.delta.storage.S3SingleDriverLogStore")
    conf.set("spark.sql.caseSensitive", "true")
    conf.set("spark.sql.execution.arrow.enabled", "true")
    #conf.set("spark.sql.shuffle.partitions",500)
    sc = SparkContext.getOrCreate()
    glueContext = GlueContext(sc)
    spark = glueContext.spark_session

    ################################ Arguments / parameter ####################################################
    # get params from Config.json
    with open('config.json', 'r') as f:
        config = json.load(f)

    if  len(sys.argv) > 1:
        args = getResolvedOptions(sys.argv, ['customer','environment','rptS3Location','s3CustomerAdl','mappingLocation'])
        customer = args['customer']
        s3_destination = args['s3CustomerAdl']
        s3_map_csv = args['mappingLocation'] + "data/mapping.csv"
        s3_load_csv = args['mappingLocation'] + "data/loading.csv"
        source_s3_location = args['rptS3Location'] + "data/archive_rds/"
        environment = args['environment']
    else:
        customer = config["customer"]
        s3_destination = config['s3_destination']
        s3_map_csv = config['s3_map_csv']
        s3_load_csv = config['s3_load_csv']
        source_s3_location = config['source_s3_location']
        environment = config['environment']
        target = config['strategy_target']

    archive_folder = config['archive_folder']
    tables = config['tables']
    bronze_tables = config['bronze_tables']

    utils = Utils(glueContext, customer)
    # version the data mapping and loading files
    df_load_pandas = utils.data_load_csv(glueContext, s3_load_csv)
    df_map_pandas = utils.data_load_csv(glueContext, s3_map_csv)
    df_map_spark = spark.createDataFrame(df_map_pandas)
    utils.df_to_delta(df_map_spark, s3_destination + "config/deltaMapping")
    print("The data mapping is versioned in Delta at "+s3_destination+"config/")
    df_load_spark = spark.createDataFrame(df_load_pandas)
    utils.df_to_delta(df_load_spark, s3_destination + "config/deltaLoading")
    print("The data loading is versioned in Delta at "+s3_destination+"config/")

    # load data from rpt S3 source and version in destination S3
    dataLoader = Data_loading(glueContext, customer,environment, df_load_pandas,df_map_pandas)
    dataLoader.run(source_s3_location, s3_destination,archive_folder, bronze_tables, tables)
    #
    target = "no"
    sugProdTrans = SuggestionsProductTrans(glueContext,customer, s3_destination)
    sugProdTrans.run()

    initRecordClass = InitRecordClass(glueContext, customer, s3_destination)
    initRecordClass.run()

    dseRecordClassTrans = DseRecordClassTrans(glueContext, customer, s3_destination)
    dseRecordClassTrans.run()
    #
    stRecordClassTransCri = StRecordClassTransCri(glueContext, customer, s3_destination, target)
    stRecordClassTransCri.run()

    finalDataTrans = FinalDataTrans(glueContext, customer, s3_destination, target)
    finalDataTrans.run()






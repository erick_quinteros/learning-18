from awsglue.transforms import *

from pyspark.sql.functions import col
from utils import Utils


# create a glue Context and connect that with spark
class InitRecordClass:
    def __init__(self, glue_context, customer, s3_destination):
        self.glueContext = glue_context
        self.customer = customer
        self.s3_destination = s3_destination
        self.utils = Utils(glue_context, customer)
        self.spark = self.glueContext.spark_session
        pass

    def run(self):
        s3_recordclasses_destination = self.s3_destination + 'data/silver/recordclasses'
        values_pd = self.spark.read.csv(self.s3_destination + 'data/silver/recordclassCsv', header='true').toPandas()
        values = dict(zip(values_pd.recordcalssId, values_pd.recordclass))
        # print(values)
        df_emails = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/emails')
        #df_emails = self.spark.read.format('parquet').load(self.s3_destination + 'data/bronze/emails/')
        df_emails.createOrReplaceTempView("emails")
        print("df_emails cahched")
        df_product_final = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/product_final')
        df_product_final.createOrReplaceTempView("product")
        print("df_product_final cached")
        df_visits = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/visits/')
        #df_visits = self.spark.read.format('parquet').load(self.s3_destination + 'data/bronze/visits/')
        df_visits.createOrReplaceTempView("visits")
        print("df_visits cached")

        df_suggestions = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/suggestions_final')
        df_suggestions.createOrReplaceTempView("suggestions")
        print("df_suggestions cached")

        df_dse = self.utils.dataLoadDelta(self.s3_destination + 'data/bronze/dse_score')
        df_dse.createOrReplaceTempView("dse_score")
        print("df_dse cached")

        value_tablename = {}
        list_email = ['COALESCE(fs.suggestedyear,fe.interactionyear) as All_effectiveyear',
                      'COALESCE(fs.suggestedmonth,fe.interactionmonth) as All_effectivemonth',
                      'COALESCE(fs.suggestedday,fe.interactionday) as All_effectiveday',
                      'fe.interactionId',
                      'fe.externalId',
                      'fe.timeZoneId as Interaction_timeZoneId',
                      'fe.interactionyear',
                      'fe.interactionmonth',
                      'fe.interactionday',
                      'fe.startDateTime Interaction_startDateTime',
                      'fe.startDateLocal as Interaction_startDateLocal',
                      'fe.interactionTypeId',
                      'fe.interactionTypeName',
                      'fe.repActionTypeId',
                      'fe.repActionTypeName',
                      'fe.duration as Interaction_Duration',
                      'fe.wasCreatedFromSuggestion as Interaction_wasCreatedFromSuggestion',
                      'fe.isCompleted as Interaction_isCompleted',
                      'fe.isDeleted as Interaction_isDeleted',
                      'fe.createdAt as Interaction_createdAt',
                      'fe.updatedAt as Interaction_updatedAt',
                      'fe.repid as Interaction_repid',
                      'fe.accountId as Interaction_accountId',
                      'fe.facilityId as Interaction_facilityId',
                      'fe.interaction_account_isdeleted',
                      'fe.raw_cs_id',
                      'fe.Email_Sent_Date_vod__c as Email_Email_Sent_Date_vod__c',
                      'fe.Capture_Datetime_vod__c as Email_Capture_Datetime_vod__c',
                      'fe.accountuid as Email_accountuid',
                      'fe.Account_Email_vod__c as Email_Account_Email_vod__c',
                      'fe.Email_Config_Values_vod__c as Email_Email_Config_Values_vod__c',
                      'fe.Email_Subject__c as Email_Email_Subject__c',
                      'fe.Email_Content_vod__c as Email_Email_Content_vod__c',
                      'fe.email_parsed_content',
                      'fe.Opened_vod__c as Email_Opened_vod__c',
                      'fe.Open_Count_vod__c as Email_Open_Count_vod__c',
                      'fe.Click_Count_vod__c as Email_Click_Count_vod__c',
                      'fe.Product_Display_vod__c as Email_Product_Display_vod__c',
                      'fe.Product_vod__c as Email_Product_vod__c',
                      'fe.Sender_Email_vod__c as Email_Sender_Email_vod__c',
                      'fe.Status_vod__c as Email_Status_vod__c',
                      'fe.email_Message_Id',
                      'fe.email_messageChannelId',
                      'fe.email_messageTopicId',
                      'fe.email_messageTopicName',
                      'fe.email_messagename',
                      'fe.email_messagedescription',
                      'fe.Call2_vod__c as Email_Call2_vod__c',
                      'null as Call_Call_Datetime_vod__c',
                      'null as Call_Call_Date_vod__c',
                      'null as Call_accountuid',
                      'null as Call_Address_Line_1_vod__c',
                      'null as Call_State_vod__c',
                      'null as Call_City_vod__c',
                      'null as Call_Zip_4_vod__c',
                      'null as Call_Zip_vod__c',
                      'null as Call_Address_Line_2_vod__c',
                      'null as Call_Next_Call_Notes_vod__c',
                      'null as Call_Call_Type_vod__c',
                      'null as Call_Attendee_Type_vod__c',
                      'null as Call_Detailed_Products_vod__c',
                      'null as Call_Submitted_By_Mobile_vod__c',
                      'fpf.productInteractionTypeId',
                      'fpf.productInteractionTypeName',
                      'fpf.messageId',
                      'fpf.messageTopicId',
                      'fpf.messageReaction',
                      'fpf.physicalMessageDesc',
                      'fpf.physicalMessageUID',
                      'fpf.quantity',
                      'fs.suggestionReferenceId',
                      'fs.suggestionUID',
                      'fs.suggestedDate',
                      'fs.channel as suggestion_channel',
                      'fs.actionTaken as suggestion_actionTaken',
                      'fs.dismissReason as suggestion_dismissReason',
                      'fs.interactionraw as suggestion_interactionraw',
                      'fs.interactionId as suggestion_interactionId',
                      'fs.repId as suggestion_repId',
                      'fs.detailRepActionTypeId as suggestion_detailRepActionTypeId',
                      'fs.detailRepActionName as suggestion_detailRepActionName',
                      'fs.messageId as suggestion_messageId',
                      'fs.messageName as suggestion_messageName',
                      'repTeamId as suggestion_repTeamId',
                      'fs.productId as suggestion_productId',
                      'fs.accountId as suggestion_accountId',
                      'fs.suggestedyear',
                      'fs.suggestedmonth',
                      'fs.suggestedday'
                      ]

        join_list_email = ''
        for each in list_email:
            join_list_email = join_list_email + ',' + each

        value_tablename['emails_1'] = '''
        from
          emails fe 
           inner join
              product fpf 
              on fe.interactionId = fpf.interactionId 
           left join
              suggestions fs 
              on fe.interactionId = fs.interactionId 
        where
           fe.isCompleted = 1 
           and fs.interactionId is null'''

        value_tablename['emails_2'] = '''
        from
          emails fe 
           inner join
              product fpf 
              on fe.interactionId = fpf.interactionId 
           left join
              suggestions fs 
              on fe.interactionId = fs.interactionId 
        where
           fe.isCompleted = 0 
           and fs.interactionId is null'''

        value_tablename['emails_3'] = '''
        from
          emails fe 
           inner join
              product fpf 
              on fe.interactionId = fpf.interactionId 
           left join
              suggestions fs 
              on fe.interactionId = fs.interactionId 
        where
           fe.isCompleted = 1 
           and fs.interactionId is not null'''

        value_tablename['emails_4'] = '''
        from
          emails fe 
           inner join
              product fpf 
              on fe.interactionId = fpf.interactionId 
           left join
              suggestions fs 
              on fe.interactionId = fs.interactionId 
        where
           fe.isCompleted = 0 
           and fs.interactionId is not null'''

        value_tablename['emails_5'] = '''
        from
           suggestions fs 
           left join
              emails fe 
              on fe.interactionId = fs.interactionId 
           left join
              product fpf 
              on fe.interactionId = fpf.interactionId 
        where
           (
              fs.interactionId is null 
              or fs.interactionraw is null
           )
           and fs.detailRepActionTypeId = 8'''
        i = 1
        spark_str = 'select distinct ' + str(i) + ' as recordclassId, \'' + values[
            str(i)] + '\' as recordclass ' + join_list_email + ' ${emails_' + str(i) + '}'
        print(spark_str)
        spark_str = self.utils.sql_to_template(spark_str, value_tablename)
        print(spark_str)
        df_r = self.spark.sql(spark_str)
        print(spark_str)
        df_r = df_r.select([col(c).cast("string") for c in df_r.columns])
        # s3_recordclass = s3_recordclasses_destination + "/rcid1/"
        #df_r.write.format("delta").mode("overwrite").partitionBy("recordclassId", "All_effectiveyearyear",
        #                                                         "All_effectiveyearmonth", "All_effectiveyearday").save(
        df_r.repartition(40).write.format("delta").mode("overwrite").partitionBy("recordclassId","All_effectiveyear").save(s3_recordclasses_destination)

        for i in range(2, 6):
            spark_str = 'select distinct ' + str(i) + ' as recordclassId, \'' + values[
                str(i)] + '\' as recordclass ' + join_list_email + ' ${emails_' + str(i) + '}'
            print(spark_str)
            spark_str = self.utils.sql_to_template(spark_str, value_tablename)
            print(spark_str)
            df_r1 = self.spark.sql(spark_str)
            print(spark_str)
            df_r1 = df_r1.select([col(c).cast("string") for c in df_r1.columns])
            # s3_recordclass = s3_recordclasses_destination + "/rcid" + str(i) + '/'
            df_r1.repartition(40).write.format("delta").mode("append").partitionBy("recordclassId","All_effectiveyear").save(s3_recordclasses_destination)
            # df_r = df_r.union(df_r1)
            print('record class saved at destination ', s3_recordclasses_destination)
            print('\n')

        # --------------------------------------------------------------------------------------------------------------------------
        # load next 5 record classes [ related to visits]

        value_tablename = {}
        list_visit = [
            'COALESCE(fs.suggestedyear,fv.interactionyear) as All_effectiveyear',
            'COALESCE(fs.suggestedmonth,fv.interactionmonth) as All_effectivemonth',
            'COALESCE(fs.suggestedday,fv.interactionday) as All_effectiveday',
            'fv.interactionId',
            'fv.externalId',
            'fv.timeZoneId as Interaction_timeZoneId',
            'fv.interactionyear',
            'fv.interactionmonth',
            'fv.interactionday',
            'fv.startDateTime Interaction_startDateTime',
            'fv.startDateLocal as Interaction_startDateLocal',
            'fv.interactionTypeId',
            'fv.interactionTypeName',
            'fv.repActionTypeId',
            'fv.repActionTypeName',
            'fv.duration as Interaction_Duration',
            'fv.wasCreatedFromSuggestion as Interaction_wasCreatedFromSuggestion',
            'fv.isCompleted as Interaction_isCompleted',
            'fv.isDeleted as Interaction_isDeleted',
            'fv.createdAt as Interaction_createdAt',
            'fv.updatedAt as Interaction_updatedAt',
            'fv.repId as Interaction_repid',
            'fv.accountId as Interaction_accountId',
            'fv.facilityId as Interaction_facilityId',
            'fv.interaction_account_isdeleted',
            'fv.raw_cs_id',
            'null as Email_Email_Sent_Date_vod__c',
            'null as Email_Capture_Datetime_vod__c',
            'null as Email_accountuid',
            'null as Email_Account_Email_vod__c',
            'null as Email_Email_Config_Values_vod__c',
            'null as Email_Email_Subject__c',
            'null as Email_Email_Content_vod__c',
            'null as email_parsed_content',
            'null as Email_Opened_vod__c',
            'null as Email_Open_Count_vod__c',
            'null as Email_Click_Count_vod__c',
            'null as Email_Product_Display_vod__c',
            'null as Email_Product_vod__c',
            'null as Email_Sender_Email_vod__c',
            'null as Email_Status_vod__c',
            'null as email_Message_Id',
            'null as email_messageChannelId',
            'null as email_messageTopicId',
            'null as email_messageTopicName',
            'null as email_messagename',
            'null as email_messagedescription',
            'null as Email_Call2_vod__c',
            'fv.Call_Datetime_vod__c as Call_Call_Datetime_vod__c',
            'fv.Call_Date_vod__c as Call_Call_Date_vod__c',
            'fv.accountuid as Call_accountuid',
            'fv.Address_Line_1_vod__c as Call_Address_Line_1_vod__c',
            'fv.State_vod__c as Call_State_vod__c',
            'fv.City_vod__c as Call_City_vod__c',
            'fv.Zip_4_vod__c as Call_Zip_4_vod__c',
            'fv.Zip_vod__c as Call_Zip_vod__c',
            'fv.Address_Line_2_vod__c as Call_Address_Line_2_vod__c',
            'fv.Next_Call_Notes_vod__c as Call_Next_Call_Notes_vod__c',
            'fv.Call_Type_vod__c as Call_Call_Type_vod__c',
            'fv.Attendee_Type_vod__c as Call_Attendee_Type_vod__c',
            'fv.Detailed_Products_vod__c as Call_Detailed_Products_vod__c',
            'fv.Submitted_By_Mobile_vod__c as Call_Submitted_By_Mobile_vod__c',
            'fpf.productInteractionTypeId',
            'fpf.productInteractionTypeName',
            'fpf.messageId',
            'fpf.messageTopicId',
            'fpf.messageReaction',
            'fpf.physicalMessageDesc',
            'fpf.physicalMessageUID',
            'fpf.quantity',
            'fs.suggestionReferenceId',
            'fs.suggestionUID',
            'fs.suggestedDate',
            'fs.channel as suggestion_channel',
            'fs.actionTaken as suggestion_actionTaken',
            'fs.dismissReason as suggestion_dismissReason',
            'fs.interactionraw as suggestion_interactionraw',
            'fs.interactionId as suggestion_interactionId',
            'fs.repId as suggestion_repId',
            'fs.detailRepActionTypeId as suggestion_detailRepActionTypeId',
            'fs.detailRepActionName as suggestion_detailRepActionName',
            'fs.messageId as suggestion_messageId',
            'fs.messageName as suggestion_messageName',
            'fs.repTeamId as suggestion_repTeamId',
            'fs.productId as suggestion_productId',
            'fs.accountId as suggestion_accountId',
            'fs.suggestedyear',
            'fs.suggestedmonth',
            'fs.suggestedday'

        ]

        join_list_visit = ''
        for each in list_visit:
            join_list_visit = join_list_visit + ',' + each

        value_tablename['visits_6'] = '''
        from
        visits fv 
           inner join
              product fpf 
              on fv.interactionId = fpf.interactionId 
           left join
              suggestions fs 
              on fv.interactionId = fs.interactionId 
        where
           fv.isCompleted = 1 
           and fs.interactionId is null'''

        value_tablename['visits_7'] = '''
        from
        visits fv 
           inner join
              product fpf 
              on fv.interactionId = fpf.interactionId 
           left join
              suggestions fs 
              on fv.interactionId = fs.interactionId 
        where
           fv.isCompleted = 0 
           and fs.interactionId is null'''

        value_tablename['visits_8'] = '''
        from
        visits fv 
           inner join
              product fpf 
              on fv.interactionId = fpf.interactionId 
           left join
              suggestions fs 
              on fv.interactionId = fs.interactionId 
        where
           fv.isCompleted = 1 
           and fs.interactionId is not null'''

        value_tablename['visits_9'] = '''
        from
        visits fv 
           inner join
              product fpf 
              on fv.interactionId = fpf.interactionId 
           left join
              suggestions fs 
              on fv.interactionId = fs.interactionId 
        where
           fv.isCompleted = 0
           and fs.interactionId is not null'''

        value_tablename['visits_10'] = '''
        from
           suggestions fs 
           left join
              visits fv 
              on fv.interactionId = fs.interactionId 
           left join
              product fpf 
              on fv.interactionId = fpf.interactionId 
        where
           (
              fs.interactionId is null 
              or fs.interactionraw is null
           )
           and fs.detailRepActionTypeId = 4'''

        for i in range(6, 11):
            spark_str = 'select distinct ' + str(i) + ' as recordclassId, \'' + values[
                str(i)] + '\' as recordclass ' + join_list_visit + ' ${visits_' + str(i) + '}'
            spark_str = self.utils.sql_to_template(spark_str, value_tablename)
            print('\n')
            df_v1 = self.spark.sql(spark_str)
            print(spark_str)
            df_v1 = df_v1.select([col(c).cast("string") for c in df_v1.columns])
            # s3_recordclass = s3_recordclasses_destination + "/rcid" + str(i) + '/'
            # df_v1.write.mode('overwrite').parquet(s3_recordclass)
            df_v1.repartition(40).write.format("delta").mode("append").partitionBy("recordclassId","All_effectiveyear").save(s3_recordclasses_destination)
            # df_r = df_r.union(df_r1)
            print('record class saved at destination ', s3_recordclasses_destination)
            print('\n')

        # df_r.write.format("delta").partitionBy("recordclassId").save(s3_recordclasses_destination)


from awsglue.utils import getResolvedOptions
from awsglue.context import GlueContext
from pyspark.sql.functions import *
import pandas as pd
global str
from utils import Utils
 
#create a glue Context and connect that with spark 
class SuggestionsProductTrans:
        def __init__(self, glue_context, customer, s3_destination):
                self.glueContext = glue_context
                self.customer = customer
                self.s3_destination = s3_destination
                self.utils = Utils(glue_context, customer)
                self.spark = self.glueContext.spark_session
                pass



        def run(self):
                s3_load_parquet_Product=self.s3_destination+'data/bronze/product/'
                df_Product= self.utils.dataLoadDelta(s3_load_parquet_Product)
                df_Product.cache()
                df_Product.createOrReplaceTempView("product")
                s3_load_parquet_Suggestions = self.s3_destination+'data/bronze/suggestions/'
                df_suggestions= self.utils.dataLoadDelta(s3_load_parquet_Suggestions)
                df_suggestions.cache()
                df_suggestions.createOrReplaceTempView("suggestions")
                print("product and suggestions cached")

        #create suggestions final table for further processing

                spark_str="""
                select *,year(suggestedDate) as suggestedyear,month(suggestedDate) as suggestedmonth,day(suggestedDate) as suggestedday from (
                select 
                suggestionReferenceId,
                max(suggestedDate) as suggestedDate,
                repUID,
                detailRepActionName as channel,
                CASE
                    WHEN MAX(actionTakenNumbers)  = 4  THEN "Accepted"
                    WHEN MAX(actionTakenNumbers) = 3 THEN "Accepted but Incomplete"
                    WHEN MAX(actionTakenNumbers) = 2 then "Dismissed"
                    ELSE "Not Acted Upon"
                    END AS actionTaken,
                CASE
                    WHEN MAX(actionTakenNumbers)  = 2  THEN agg.dismissReasonType
                    ELSE "NA"
                    END AS dismissReason,
                
                COALESCE(reportedInteractionUID,inferredInteractionUID,interactionUID) as interactionraw,interactionId,
                repId,detailRepActionTypeId,detailRepActionName,concat_ws(',', collect_list(distinct repTeamId)) as repTeamId,messageId,messageName,concat_ws(',', collect_list(distinct productId)) as productId,concat_ws(',', collect_list(distinct productName)) as productName,accountId,suggestionUID
                from (
                select DATE(CONCAT(DATE_FORMAT(startDateLocal, '%Y-%m'), '-01')) as periodValue,
                dm_suite_suggestions.*,
                CASE WHEN actionTaken ='No Action Taken' then 1
                    WHEN actionTaken ='Suggestions Dismissed' then 2 
                    WHEN actionTaken = 'Suggestions Completed' and (ISNULL(isCompleted) or isCompleted = 0) then 3
                    WHEN actionTaken = 'Suggestions Completed' and isCompleted = 1 and startDateTime < NOW() then 4
                    END AS actionTakenNumbers
                from suggestions dm_suite_suggestions
                ) agg
                   GROUP BY  dismissReasonType,repUID,suggestionReferenceId,messageId,messageName,COALESCE(reportedInteractionUID,inferredInteractionUID,interactionUID),interactionId,
                    repId,detailRepActionTypeId,detailRepActionName,accountId,suggestionUID
                ) t   
                    
                """

                df=self.spark.sql(spark_str)
                df.repartition("suggestedyear", "suggestedmonth", "suggestedday").write.mode('overwrite').partitionBy("suggestedyear", "suggestedmonth", "suggestedday").format("delta").save(self.s3_destination+'data/bronze/suggestions_final/')


                spark_str="""select ip.interactionId,ip.productInteractionTypeId,ip.productInteractionTypeName,concat_ws(',', collect_list(ip.messageId)) as messageId, 
                concat_ws(',', collect_list(ip.messageTopicId)) as messageTopicId, 
               -- concat_ws(',', collect_list(ip.messageTopicName)) as messageTopicName, 
                concat_ws(',', collect_list(ip.physicalMessageName)) as physicalMessageName, 
                concat_ws(',', collect_list(ip.physicalMessageDesc)) as physicalMessageDesc, 
                concat_ws(',', collect_list(ip.physicalMessageUID)) as physicalMessageUID, 
                concat_ws(',', collect_list(ip.suggestionReferenceId)) as suggestionReferenceId, 
                concat_ws(',', collect_list(ip.matchedSuggestionProduct)) as matchedSuggestionProduct, 
                concat_ws(',', collect_list(ip.matchedSuggestionMessage)) as matchedSuggestionMessage, 
                concat_ws(',', collect_list(ip.suggestionInferredAt)) as suggestionInferredAt, 
                concat_ws(',', collect_list(ip.messageReaction)) as messageReaction, 
                concat_ws(',', collect_list(ip.quantity)) as quantity,
                
                sum(case when (productId=1001 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1001 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1001`,
                
                sum(case when (productId=1002 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1002 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1002`,
                
                sum(case when (productId=1003 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1003 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1003`,
                
                sum(case when (productId=1004 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1004 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1004`,
                
                sum(case when (productId=1006 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1006 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1006`,
                
                sum(case when (productId=1007 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1007 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1007`,
                
                sum(case when (productId=1008 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1008 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1008`,
                
                sum(case when (productId=1009 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1009 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1009`,
                
                sum(case when (productId=1010 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1010 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1010`,
                
                sum(case when (productId=1011 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1011 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1011`,
                
                sum(case when (productId=1012 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1012 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1012`,
                
                sum(case when (productId=1013 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1013 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1013`,
                
                sum(case when (productId=1014 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1014 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1014`,
                
                sum(case when (productId=1015 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1015 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1015`,
                
                sum(case when (productId=1016 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1016 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1016`,
                
                sum(case when (productId=1017 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1017 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1017`,
                
                sum(case when (productId=1018 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1018 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1018`,
                
                sum(case when (productId=1019 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1019 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1019`,
                
                sum(case when (productId=1025 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1025 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1025`,
                
                sum(case when (productId=1 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=1 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `1`,
                
                sum(case when (productId=2 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=2 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `2`,
                sum(case when (productId=3 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=3 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `3`,
                sum(case when (productId=4 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=4 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `4`,
                sum(case when (productId=5 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=5 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `5`,
                sum(case when (productId=6 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=6 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `6`,
                sum(case when (productId=7 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=7 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `7`,
                sum(case when (productId=8 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=8 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `8`,
                sum(case when (productId=9 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=9 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `9`,
                sum(case when (productId=10 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=10 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `10`,
                sum(case when (productId=11 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=11 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `11`,
                sum(case when (productId=12 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=12 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `12`,
                sum(case when (productId=13 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=13 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `13`,
                sum(case when (productId=14 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=14 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `14`,
                sum(case when (productId=15 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=15 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `15`,
                sum(case when (productId=16 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=16 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `16`,
                sum(case when (productId=17 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=17 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `17`,
                sum(case when (productId=18 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=18 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `18`,
                sum(case when (productId=19 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=19 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `19`,
                sum(case when (productId=20 and isCompleted=1)  then 1+ifnull(actionOrder,0) when  (productId=20 and isCompleted=0) then ifnull(actionOrder,0)+0.5 end) as `20`
                
                
                from product ip  group by ip.interactionId,ip.productInteractionTypeId,ip.productInteractionTypeName 
                """

                df_prod=self.spark.sql(spark_str)
                df_prod.coalesce(1).write.mode('overwrite').format("delta").save(self.s3_destination+'data/bronze/product_final/')


                #store recordclasses as csv file
                data = {'recordcalssId': ['1', '2', '3','4', '5', '6','7', '8', '9','10'],
                        'recordclass': ['Email only with Interaction completed','EMail only with Interaction not_completed',
                        'Email and suggestion with Interaction completed','Email and suggestion with Interaction not_completed',
                        'EMail suggestion without Interactiond','Visit only with Interaction completed',
                        'Visit only with Interaction not_completed','Visit and suggestion with Interaction completed',
                        'Visit and suggestion with Interaction not_completed','Visit suggestion without Interaction']
                }

                df = pd.DataFrame(data)
                ddf = self.spark.createDataFrame(df)
                ddf.coalesce(1).write.mode("overwrite").format('com.databricks.spark.csv').save(self.s3_destination+'data/silver/recordclassCsv', header='true')



#### this function equal to the following expr
### AP <- AP[,sapply(AP,function(x)length(unique(x))>1),with=F]   # pick out variable withs more than 1 unique value (also drop productName column here)
sparkFilterOutColSameValue <- function(df) {
  distinctCount <- df %>% mutate_all(funs(approx_count_distinct(.))) %>% head(1) %>% as.data.frame()
  cols2PlusDistinct <- names(distinctCount)[sapply(distinctCount, function(x){x>1})]
  df <- df %>% select(one_of(c(cols2PlusDistinct)))
  return(df)
}
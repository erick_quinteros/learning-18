##########################################################
##
##
## aktana-learning Install Aktana Learning Engines.
##
## description: database connection common function
##
## created by : marc.cohen@aktana.com
## updated by : shirley.xu@aktana.com
##
## created on : 2015-11-03
## updated on : 2018-09-17
##
## Copyright AKTANA (c) 2018.
##
##
##########################################################


################################################
## function: get connection handle of DSE DB
################################################
getDBConnection <- function(dbuser, dbpassword, dbhost, dbname, port)
{
  drv <- dbDriver("MySQL")
  tryCatch(con <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbname,port=port), 
           error = function(e) {
             flog.error('Error in connecting to db %s: %s', dbname, e, name='error')
             quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
           })
  return(con) 
}

#################################################
## function: get connection handle of learning DB
#################################################
getDBConnectionLearning <- function(dbuser, dbpassword, dbhost, dbname_learning, port)
{
  con_l <- getDBConnection(dbuser, dbpassword, dbhost, dbname_learning, port)
  return(con_l) 
}

#################################################
## function: get connection handle of _stage DB
#################################################
getDBConnectionStage <- function(dbuser, dbpassword, dbhost, dbname_stage, port)
{
  con_stage <- getDBConnection(dbuser, dbpassword, dbhost, dbname_stage, port)
  return(con_stage) 
}

#################################################
## function: get connection handle of _cs DB
#################################################
getDBConnectionCS <- function(dbuser, dbpassword, dbhost, dbname_cs, port)
{
  con_cs <- getDBConnection(dbuser, dbpassword, dbhost, dbname_cs, port)
  return(con_cs) 
}

#################################################
## function: get connection handle of _archive DB
#################################################
getDBConnectionArchive <- function(dbuser, dbpassword, dbhost, dbname, port)
{
  dbname_archive <- sprintf("%s_archive", dbname)
  con_archive <- getDBConnection(dbuser, dbpassword, dbhost, dbname_archive, port)
  return(con_archive) 
}
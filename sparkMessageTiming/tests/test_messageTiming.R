context('testing messageTiming() func in Spark TTE module')
print(Sys.time())

# load packages and scripts needed for running tests
library(h2o)
library(uuid)
library(Learning)
library(data.table)
library(sparkLearning)
library(sparklyr)
library(dplyr)
library(openssl)

source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
source(sprintf("%s/sparkMessageTiming/messageTiming.R",homedir))
source(sprintf("%s/sparkMessageTiming/getTTEparams.R",homedir))
source(sprintf("%s/sparkMessageTiming/buildStaticFeatures.R",homedir))

libdir <<- sprintf("%s/library", homedir)

# set up unit test build folder and related configs
# get buildUID
BUILD_UID <<- readModuleConfig(homedir, 'messageTiming','buildUID')

print(BUILD_UID)
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'messageTiming', BUILD_UID, files_to_copy='learning.properties')

# start h2o
print("start h2o early")
tryCatch(suppressWarnings(h2o.init(nthreads=-1, max_mem_size="8g")), 
         error = function(e) {
           flog.error('Error in h2o.init()',name='error')
           quit(save = "no", status = 65, runLast = FALSE) # user-defined error code 65 for failure of h2o initialization
         })

# load input data from loadMessageTimingData
load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_interactions.RData', homedir))
load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_accountProduct.RData', homedir))
load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_products.RData', homedir))
load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_accounts.RData', homedir))

# initialize config
# parameter defaults (needed for func in learning package work)
DRIVERMODULE <- "messageTimingDriver.r"
RUN_UID <- UUIDgenerate()
runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=NULL, createExcel=TRUE, createPlotDir=TRUE, createModelsaveDir=TRUE)
config <- initializeConfigurationNew(homedir, BUILD_UID)
modelSaveDir <- sprintf("%s/builds/%s/models_%s", homedir, BUILD_UID, BUILD_UID)

runDir <<- runSettings[["runDir"]]
WORKBOOK <<- runSettings[["WORKBOOK"]]
runStamp <<- BUILD_UID 

VERSION_UID <- config[["versionUID"]]
CONFIG_UID <- config[["configUID"]]

tteParams <- getTTEparams(config, "BUILD")

con   <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

sc <- initializeSpark(homedir, master="local", version="2.3.1")

interactions[, date := as.character(date)]  # for conversion in spark
interactions <- sdf_copy_to(sc, interactions, "interactions", overwrite=TRUE, memory=FALSE)
interactions <<- interactions %>% dplyr::mutate(date = to_date(date))

accountProduct <<- sdf_copy_to(sc, accountProduct, "accountProduct", overwrite=TRUE, memory=FALSE)
products <<- sdf_copy_to(sc, products, "products", overwrite=TRUE, memory=FALSE)
accounts <<- sdf_copy_to(sc, accounts, "accounts", overwrite=TRUE, memory=FALSE)

# run messageSequence
result <- messageTiming(sc, con, con_l, tteParams)

spreadsheetName <- sprintf("%s/%s_%s.xlsx", runDir, DRIVERMODULE, runStamp)

statusCode <- tryCatch(saveWorkbook(WORKBOOK, file = spreadsheetName, overwrite = T), 
    error = function(e) {
    flog.info("Original error message: %s", e)
    flog.error('Error in saveWorkbook',name='error')
    return (NA)
})

if (is.na(statusCode)) {
    quit(save = "no", status = 72, runLast = FALSE)   # user-defined error code 72 for memory error
}

pNameUID <- tteParams[["pNameUID"]]
pName <- products[externalId==pNameUID]$productName

sheetname.A <- sprintf("%s_A", pName)
importance <- data.table(read.xlsx(spreadsheetName, sheet=sheetname.A))

sheetname.A.ref <- sprintf("%s_A_reference", pName)
accuracy <- data.table(read.xlsx(spreadsheetName,sheet=sheetname.A.ref))

# test cases
test_that("result has the correct output", {
  expect_equal(dim(accuracy), c(1, 5))
  expect_equal(dim(importance), c(20, 2))

  # compare with save results
  load(sprintf('%s/messageTiming/tests/data/from_messageTiming_models.RData', homedir))
  load(sprintf('%s/messageTiming/tests/data/from_messageTiming_output.RData', homedir))

  #expect_setequal(importance$names, output$names)
  expect_equal(accuracy$target, models$target)
  
  eps <- abs(accuracy$MSE - models$MSE)
  expect_lte(eps, 0.0001)
})

# test file structure
test_that("build dir has correct struture", {
  expect_file_exists(sprintf('%s/builds/',homedir))
  expect_file_exists(sprintf('%s/builds/%s',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/learning.properties',homedir,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,BUILD_UID,paste("Build",RUN_UID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,BUILD_UID,paste("Build",RUN_UID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/models_A_%s',homedir,BUILD_UID,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/models_B_%s',homedir,BUILD_UID,BUILD_UID))
  expect_file_exists(sprintf('%s/builds/%s/models_C_%s',homedir,BUILD_UID,BUILD_UID))
  expect_num_of_file(sprintf('%s/builds/%s/models_A_%s',homedir,BUILD_UID,BUILD_UID),1)
  expect_num_of_file(sprintf('%s/builds/%s/models_B_%s',homedir,BUILD_UID,BUILD_UID),1)
  expect_num_of_file(sprintf('%s/builds/%s/models_C_%s',homedir,BUILD_UID,BUILD_UID),1)
})

# run test cases on log file contents
# read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
log_contents <- readLogFile(homedir,BUILD_UID,paste("Build",RUN_UID,sep="_"))

test_that("check processing AccountProduct/Account predictors", {
  ind <- grep('Finished training model-A', log_contents)
  expect_length(ind, 1)
  
  ind <- grep("Finished building dynamic model", log_contents)
  expect_length(ind, 1)
})

# final clean up
closeClient()

